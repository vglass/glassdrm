#ifndef OXTDRM_PV__H
#define OXTDRM_PV__H

#include "oxtdrm_drv.h"
int32_t oxtdrm_init_pv_display(struct oxtdrm_device *device);
int32_t oxtdrm_reconnect_provider(struct pv_display_provider *provider);
void oxtdrm_teardown_provider(void);

void oxtdrm_destroy_display(uint32_t key);
void oxtdrm_destroy_displays(void);

int32_t oxtdrm_reconnect_display(struct pv_display *display, struct dh_add_display *request);

void oxtdrm_host_display_changed(struct pv_display_provider *provider,
                        struct dh_display_info *displays, uint32_t num_displays);
void oxtdrm_add_display_request(struct pv_display_provider *provider, struct dh_add_display *request);
#endif // OXTDRM_PV__H
