Dynamic Debugging
-----------------


By default, log messages will be disabled for this module. However, it can be turned on at anytime to produce useful information. The linux dynamic debugging interface gives us flexibility and control in regards to the logs that we can display.

**Format**

  `#echo "<matches> <ops><flags>" > <debugfs>/dynamic_debug/control`
    
**Matches**

`file` string  
`func` string  
`line` line-range  
`module` string  

**Flags**

`f` - Include the function name in the printed message  
`l` - Include line number in the printed message  
`m` - Include module name in the printed message  
`p` - Show printk messages in dmesg  
`t` - Include thread ID in messages  

**Ops**

`-` Removes the given flag  
`+` Add the given flag  
`=` Set the flags to the given flags  

**Examples**


**Print All Log Messages for oxtdrm**  
  * `echo 'module oxtdrm +p' > /sys/kernel/debug/dynamic_debug/control`

**Print all log messages for oxtdrm and include the function name, line number, and the module name in the output**  
  * `echo 'module oxtdrm =pflm' > /sys/kernel/debug/dynamic_debug/control`

**Disable all flags**  
  * `echo 'module oxtdrm -pflm' > /sys/kernel/debug/dynamic_debug/control`

**Only print log messages in file oxtdrm_kms.c**  
  * `echo 'file oxtdrm_kms.c +p' > /sys/kernel/debug/dynamic_debug/control`
    
**Disable log messages in file oxtdrm_kms.c**  
  * `echo 'file oxtdrm_kms.c -p' > /sys/kernel/debug/dynamic_debug/control`
    
**Print line 78 from file oxtdrm_fb.c**  
  * `echo 'file oxtdrm_fb.c line 78 +p' > /sys/kernel/debug/dynamic_debug/control`
    
**Print log messages in the function oxtdrm_crtc_cursor_set2**  
  * `echo 'func oxtdrm_crtc_cursor_set2 +p' > /sys/kernel/debug/dynamic_debug/control`

**Different ways to enable boot time logging**  
  1. Create /etc/modprobe.d/oxtdrm.conf with the following:  
    * `options oxtdrm dyndbg=+p`  
  2. Add the following boot parameter  
    * `oxtdrm.dyndbg=+p`
