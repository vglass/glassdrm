//
// OXTdrm: OpenXT's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#include "oxtdrm_drv.h"

struct drm_gem_object*
oxtdrm_gem_object_lookup(struct drm_device *dev, struct drm_file *file, uint32_t handle)
{
    // Checks so see if we are running Rhel which runs on the 3.10 kernel.
    // Special case due to rhel backporting most of it's stuff.
    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,4)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 7, 0))
        (void)dev;
        return drm_gem_object_lookup(file, handle);
    #else
        return drm_gem_object_lookup(dev, file, handle);
    #endif
}

/**
 *  oxtdrm_dumb_map_offset
 *
 * Creates a fake offset for the dumb buffer's GEM object so that it
 * can later be mapped into memory using that offset.
 *
 * @param file_priv  The drm_file that is associated with the GEM object.
 * @param dev        The drm device that is associated with the GEM object
 * @param handle     The handle that was assigned to the GEM object
 * @param offset     Out parameter that stores that fake offset that gets created
 * @return 0 on successs, error code otherwise.
 */
int32_t
oxtdrm_dumb_map_offset(struct drm_file *file_priv, struct drm_device *dev,
                uint32_t handle, uint64_t *offset)
{
        struct drm_gem_object *obj = NULL;
        int32_t rc;

        __OXTDRM_TRACE__;

        // Validate paramters
        if (!file_priv || !dev || handle <= 0)
                return -EINVAL;

        //mutex_lock(&dev->struct_mutex);

        // Obtain drm_gem_object from the drm device and file.
        // Then convert that object to an oxt_gem_object
        obj = oxtdrm_gem_object_lookup(dev, file_priv, handle);
        if (!obj) return -EINVAL;

        // Prior to creating a fake offset for the gem object,
        // we must verify that one is not already created.

    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,4)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 7, 0))
        if (!obj->filp) {
           rc = -EINVAL;
           goto out;
        }

        rc = drm_gem_create_mmap_offset(obj);
        if (rc) goto out;
    #else
        if (!drm_vma_node_has_offset(&obj->vma_node))
        {
                // Associate the GEM object with a fake offet.
                // That way we can issue an mmap call and use
                // this offset for the offset argument.
                rc = drm_gem_create_mmap_offset(obj);

                // If for some reason we were not able to create
                // the fake offset, fail out.
                if (rc)
                        goto out;
        }
    #endif

        // Set the fake offset for gem object
        *offset = drm_vma_node_offset_addr(&obj->vma_node) + (handle<<PAGE_SHIFT);

out:
        drm_gem_object_release(obj);
        //mutex_unlock(&dev->struct_mutex);
        return rc;
}


int32_t
mmap_dumb_buffer(struct oxtdrm_gem_object *oxtgem, struct vm_area_struct *vma)
{
	uintptr_t next_to_map;
    struct page *page_to_map;
    uintptr_t next_virtual_address;
    size_t size;
	int rc = 0, i = 0;

	__OXTDRM_TRACE__;

	if (!oxtgem) {
		return -EINVAL;
    }

    // Get the starting address for the virtual memory region.
    next_virtual_address = vma->vm_start;

    // Determine the size of the region to be allocated.
    size = vma->vm_end - vma->vm_start;

	if (oxtgem->image) {
            next_to_map = (uintptr_t)oxtgem->image;
	} else {
            oxtdrm_debug("Invalid memory in gem object\n");
            return -EINVAL;
	}

    // Continue mapping until we've used up all data to map.
	for(i = 0; i < size; i+=PAGE_SIZE, next_to_map+=PAGE_SIZE, next_virtual_address+=PAGE_SIZE)
	{
		// Get a reference to the next page to be mapped...
		page_to_map = vmalloc_to_page((void *)next_to_map);

		// If we received an invalid page, fail o ut
		if (!page_to_map)
		{
			oxtdrm_error("Invalid page to map: %p\n", (void*)next_to_map);
			return -EINVAL;
		}

		// Map memory into userspace
		rc = vm_insert_page(vma, next_virtual_address, page_to_map);

		// If we failed, return the relevant error code.
		if (rc)
		{
			oxtdrm_debug("%s - Failed to insert page\n", __FUNCTION__);
			return rc;
		}
	}

	return rc;
}

/**
 *  oxtdrm_gem_mmap
 *
 *  A wrapper around the drm_gem_mmap call that also modifies a couple
 *  of flags so that it can work smoothly with our memory pool. The GEM object
 *  is located based on the fake file offset that was created, then that memory
 *  is mapped.
 *
 * @param filp  The file structure that is associated with this driver
 * @param vma    The virtual memory area that we are mapping into.
 * @return 0 on successs, error code otherwise.
 */
int32_t
oxtdrm_gem_mmap(struct file *filp, struct vm_area_struct *vma)
{
    int32_t rc = 0;
    struct drm_file *file_priv = NULL;
    uint32_t handle = 0;
    struct drm_device *device = NULL;
    struct drm_gem_object *gem = NULL;
    struct oxtdrm_gem_object *oxtgem = NULL;

    __OXTDRM_TRACE__;

    // Validate parameters
    if (!filp || !vma)
        return -ENOENT;

    // Get a shortcut to the oxtdrm device
    file_priv = filp->private_data;
    device = file_priv->minor->dev;

    handle = vma->vm_pgoff & 0xffff;

    if(device) {
        gem = oxtdrm_gem_object_lookup(device, file_priv, handle);
        if (!gem) return -EINVAL;

        oxtgem = to_oxtdrm_gem_object(gem);
        if (!oxtgem) return -EINVAL;
    } else {
        oxtdrm_debug("Couldn't find drm_device %x\n", handle);
    }

	if(oxtgem) {
        rc = mmap_dumb_buffer(oxtgem, vma);
	} else {
		rc = drm_gem_mmap(filp, vma);
	}

    return rc;
}

/**
 *  oxtdrm_gem_create
 *
 * Creates storage for the gem object. After it is created the normal gem
 * object is then stored insided the custom oxtgem object.
 * The oxt specific gem object subclassed the regular drm_gem_object in
 * order to extend its features by
 * providing additional fields to hold more information. This will act as the
 * main gem object.
 *
 * @param device  The main drm device structure, that is associate
 *                with the regular gem object
 * @param size The size in which to make the new oxt_gem object
 * @return A new drm_gem_object on success, error code otherwise
 */
static struct drm_gem_object *
__oxtdrm_gem_create(struct drm_file *file, struct drm_device *device,
                uint32_t size)
{
    struct oxtdrm_gem_object *oxtgem = NULL;
    struct drm_gem_object *gem = NULL;

    __OXTDRM_TRACE__;

    // Validate Parameters
    if (!device || (size == 0))
            return ERR_PTR(-EINVAL);

    // Initialize space for our specific oxtGem. Zero out this memory
    oxtgem = kzalloc(sizeof(*oxtgem), GFP_KERNEL);

    // If we were not able to allocate
    // memory for out private oxtgem object, fail out
    if (!oxtgem)
        return ERR_PTR(-ENOMEM);

    // The actual GEM object can now point to the allocated space for the
    // embedded GEM object within out specific oxtGEMObject, which is base
    gem = &oxtgem->base;

    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE != RHEL_RELEASE_VERSION(8,0))
        // Setup the gem object callbacks
        gem->funcs = &oxtdrm_gem_object_funcs;
    #endif

    // Round our size to the nearest page size
    size = round_up(size, PAGE_SIZE);

    // Initialize the GEM object. This will create a shmfs file of size size
    // and store it in the filep field of gem
    if (drm_gem_object_init(device, gem, size))
    {
        kfree(oxtgem);
        return NULL;
    }

    // Create a way of accessing the oxtdrm_device
    // from both the GEM object and drm_file
    oxtgem->oxtdrm = (struct oxtdrm_device *)device->dev_private;
    oxtgem->type = OXTDRM_ERROR;
    oxtgem->image = NULL;

    oxtdrm_debug("%s - GEM Object created\n", __FUNCTION__);
    return gem;
}

/**
 *  oxtdrm_dumb_create
 *
 *  Create a GEM object that is compatible for scanout with the appropriate
 *  width, height, and depth that is supplied from the
 *  userpace via drm_mode_create_dumb. this will also fill out the handle,
 *  size and pitch of the new gem object, making it available
 *  in userspace.
 *
 * @param file The drm file to associate the gem object with
 * @param dev  The main drm device structure, that will be pinned to the
 *             gem object
 * @param args Userspace arguments that holds some preferences on how
 *              to create the dumb buffer
 * @return 0 on success, error code otherwise
 */
int32_t
oxtdrm_dumb_create(struct drm_file *file, struct drm_device *dev,
                struct drm_mode_create_dumb *args)
{
    struct drm_gem_object *gem = NULL;
    struct oxtdrm_gem_object *oxtgem = NULL;
    struct oxtdrm_device *device = NULL;
    int32_t rc = 0;
    u32 handle;

    __OXTDRM_TRACE__;

    // Validate parameters
    if (!file || !dev || !args)
        return -EINVAL;

    // Get the oxtdrm device from the drm device
    // so we can populate the display field.
    device = dev->dev_private;

    // Ensure that we actually got a oxtdrm device
    if (!device)
        return -ENOENT;

    oxtdrm_debug("Creating a Buffer of %dx%d\n", args->width, args->height);

    // Calculate the pitch and size
    args->pitch = args->width * ((args->bpp + 7) / 8);
    //args->pitch = args->width * 4;
    args->size = (args->height * args->pitch);

    oxtdrm_debug("Dumb debug pitch: %d\n", args->pitch);
    // Validate that we have a good size, if not exit out
    if (args->size == 0)
        return -EINVAL;

    // Create and initialize a gem object
    gem = __oxtdrm_gem_create(file, dev, args->size);

    // If we were not able to create the gem object then fail out
    // Since the dumb buffer needs to be backed by the gem object
    if (!gem)
        return -ENOMEM;

    // Distinguish this gem object's type
    oxtgem = to_oxtdrm_gem_object(gem);
    if(args->height == 64 && args->width == 64) {
        oxtgem->type = OXTDRM_CURSOR;
        oxtgem->image = vmalloc(args->size);
        if(!oxtgem->image) {
            return -ENOMEM;
        }
    } else {
        oxtgem->type = OXTDRM_SCANOUT;
        oxtgem->image = vmalloc(args->size);

        if(!oxtgem->image) {
            return -ENOMEM;
        }
    }

    // Get a handle for the gem object, which registers a handle reference for
    // the object.
    rc = drm_gem_handle_create(file, gem, &handle);

    // If we are not able to create a handle for the gem object, fail out.
    if (unlikely(rc)) {
        oxtgem->type = -1;
        vfree(oxtgem->image);
    }

    // Let the handle be known in userspace
    args->handle = handle;

    __OXTDRM_TRACE__;
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7,6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 20, 0))
    drm_gem_object_unreference_unlocked(gem);
#elif (RHEL_RELEASE_CODE != 0 && (RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(8,4)))
    drm_gem_object_put(gem);
#else
    drm_gem_object_put_unlocked(gem);
#endif
    return rc;
}

int oxtdrm_dumb_destroy(struct drm_file *file, struct drm_device *dev, uint32_t handle)
{
    struct drm_gem_object *gem = NULL;
    struct oxtdrm_gem_object *oxtgem = NULL;
    struct oxtdrm_device *device = NULL;
    int32_t rc = 0;

    __OXTDRM_TRACE__;

    // Validate parameters
    if (!file || !dev || !handle)
        return -EINVAL;

    // Get the oxtdrm device from the drm device
    // so we can populate the display field.
    device = dev->dev_private;

    // Ensure that we actually got a oxtdrm device
    if (!device)
        return -ENOENT;

    // Create and initialize a gem object
    gem = oxtdrm_gem_object_lookup(dev, file, handle);

    // If we were not able to create the gem object then fail out
    // Since the dumb buffer needs to be backed by the gem object
    if (!gem)
        return -ENOENT;

    // Distinguish this gem object's type
    oxtgem = to_oxtdrm_gem_object(gem);
    if (!oxtgem)
        return -ENOENT;

    if (!oxtgem->image) {
        return -ENOENT;
    }

    vfree(oxtgem->image);
    oxtgem->image = NULL;

    return rc;
}

void
oxtdrm_gem_free_object(struct drm_gem_object *object)
{
    struct oxtdrm_gem_object *oxtgem = to_oxtdrm_gem_object(object);

    __OXTDRM_TRACE__;

    switch(oxtgem->type) {
        case OXTDRM_SCANOUT:
        {
            // Scanout specific cleanup here
            break;
        }
        case OXTDRM_CURSOR:
        {
            // Cursor specific cleanup here
            break;
        }
        case OXTDRM_ERROR:
        default:
        {
            break;
        }
    }

    vfree(oxtgem->image);

    // First release GEM buffer object resources
    if (object)
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7,6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 20, 0))
        drm_gem_object_unreference_unlocked(object);
#elif (RHEL_RELEASE_CODE != 0 && (RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(8,4)))
        drm_gem_object_put(object);
#else
        drm_gem_object_put_unlocked(object);
#endif

    // Free the custom GEM object, which had the drm_gem object embedded inside.
    if (oxtgem)
        kfree(oxtgem);

    return;
}

const struct vm_operations_struct oxtdrm_gem_vm_ops = {
    .open  = drm_gem_vm_open,
    .close = drm_gem_vm_close,
};

#if (RHEL_RELEASE_CODE != 0 && (RHEL_RELEASE_CODE != RHEL_RELEASE_VERSION(8,0)))
const struct drm_gem_object_funcs oxtdrm_gem_object_funcs = {
    .free = oxtdrm_gem_free_object,
    .vm_ops = &oxtdrm_gem_vm_ops,
};
#endif
