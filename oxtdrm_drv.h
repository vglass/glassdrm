//
// OXTdrm: OpenXT's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#ifndef OXTDRM_IOCTL_H
#define OXTDRM_IOCTL_H

#include <drm/drm_atomic.h>
#include <drm/drm_atomic_helper.h>
#include <drm/drm_gem.h>
#include <drm/drm_crtc.h>
#include <drm/drm_crtc_helper.h>
#include <drm/drm_fb_helper.h>
#include <drm/drm_plane_helper.h>
#include <pv_display_helper.h>
#include <linux/version.h>
#include <linux/hashtable.h>
#include <linux/ioctl.h>

#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(8,3)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(5, 0, 0))
    #include <drm/drm_drv.h>
    #include <drm/drm_prime.h>
    #include <drm/drm_ioctl.h>
    #include <drm/drm_sysfs.h>
    #include <drm/drm_vblank.h>
    #include <drm/drm_device.h>
    #include <linux/platform_device.h>
    #include <linux/kernel.h>
    #include <linux/mutex.h>
    #include <linux/slab.h>
    #include <linux/types.h>
    #include <linux/file.h>
#else
    #include <drm/drmP.h>
#endif

/* Define all of the driver information */
#define DRIVER_NAME      "oxtdrm"
#define DRIVER_MAJOR     3
#define DRIVER_MINOR     0
#define DRIVER_MICRO     6
#define DRIVER_DESC      "Custom DRM driver for OpenXT"
#define DRIVER_AUTHOR    "Maurice Gale"
#define DRIVER_DATE      "20160310"
#define DRIVER_LICENSE   "GPL"
#define OXTDRM_HASH_BUCKET_ORDER 4

/* Mode Configuration Information */
#define MODE_MIN_HEIGHT 0
#define MODE_MIN_WIDTH 0
#define MODE_4K_WIDTH 3840
#define MODE_4K_HEIGHT 2160
#define MODE_HD_WIDTH 1920
#define MODE_HD_HEIGHT 1200
#define MODE_DEFAULT_BPP 32
#define MODE_DEFAULT_DEPTH 24
#define MODE_DEFAULT_SHADOW 0
#define MODE_DEFAULT_WIDTH 1024
#define MODE_DEFAULT_HEIGHT 768
#define CURSOR_WIDTH 64
#define CURSOR_HEIGHT 64
#define CURSOR_SIZE CURSOR_WIDTH * CURSOR_HEIGHT * 4 + 4096 * 2

/* FBDEV Emnulation Information */
#define FB_MAX_CRTC_SUPPORT 1
#define FB_MAX_CONNECTOR_COUNT 1

/* OXT Specific */
#define DISPLAY_HANDLER_PORT 1000
#define DOMAIN_0 0
#define DELAY 1000
#define MAX_DISPLAYS 6
#define RECONNECT_DELAY 500
#define MAX_NAME_SIZE 32

/* Dynamic Debugging bits */
#define oxtdrm_debug(...) do {pr_debug("oxtdrm[debug]: " __VA_ARGS__); } while(0);
#define __OXTDRM_TRACE__  do {pr_debug("TRACE: %s:%i\n", __func__, __LINE__); } while(0);

/* Always print error and info messages */
#define oxtdrm_error(...) printk(KERN_ERR "oxtdrm[error]: " __VA_ARGS__)
#define oxtdrm_info(...) printk(KERN_ERR "oxtdrm[info]: " __VA_ARGS__)

// IOCTL Bits
#define OXTDRM_IOCTL_OFFSET 0x30
#define OXTDRM_IOCTL_GET_STATUS        _IOR('S', 1, uint32_t)
#define OXTDRM_IOCTL_GET_DISPLAY_INFO  _IOR('S', 2, uint32_t)
#define OXTDRM_IOCTL_UPDATE_GUEST_LAYOUT  _IOR('S', 3, uint32_t)
#define OXTDRM_IOCTL_GET_HOST_LAYOUT_CHANGED  _IOR('S', 4, uint32_t)

#ifndef RHEL_RELEASE_CODE
#define RHEL_RELEASE_CODE 0
#define RHEL_RELEASE_VERSION(a,b) (((a) << 8) + (b))
#endif

enum oxtdrm_driver_status {
    OXTDRM_STATUS_UNINITIALIZED,
    OXTDRM_STATUS_INITIALIZED,
    OXTDRM_STATUS_BUSY,
    OXTDRM_STATUS_READY,
    OXTDRM_STATUS_UNKNOWN,
};

enum oxtdrm_types {
    OXTDRM_PCI,
    OXTDRM_UNKNOWN,
};

enum oxtdrm_gem_type {
    OXTDRM_ERROR,
    OXTDRM_SCANOUT,
    OXTDRM_CURSOR,
    __OXTDRM_GEM_INVALID,
};

enum oxtdrm_buffer_type {
    OXTDRM_BUFFER_1080,
    OXTDRM_BUFFER_4K,
    __OXTDRM_BUFFER_INVALID,
};

/**
* Extend to functionality of the drm_gem_object by creating a new structure
* that will be driver specific and contains additional fields,
* and then embed the drm_gem_object inside of
* the structure (base).
*/
struct oxtdrm_gem_object
{
    struct drm_gem_object base;
    enum oxtdrm_gem_type type;
    struct oxtdrm_device *oxtdrm;
    uint8_t *image;
};

// Helper Macro to convert a regular GEM object to a custom oxtdrm_gem_object.
#define to_oxtdrm_gem_object(x) container_of(x, struct oxtdrm_gem_object, base)

struct oxt_cursor {
	uint32_t x, y, w, h;
	uint8_t *image;
};

struct oxtdrm_framebuffer {
    struct drm_framebuffer base;
    struct oxtdrm_device *device;
    struct drm_gem_object *obj;
    struct pv_display *display;
    struct oxt_cursor *cursor;
    uint32_t fb_id;
    spinlock_t lock;
};

// Helper to convert a stander drm_framebuffer object to an oxtdrm specific one
#define to_oxtdrm_framebuffer(x) container_of(x, struct oxtdrm_framebuffer, base)

struct oxtdrm_work {
    struct work_struct reconnect_task;
    struct oxtdrm_device *device;
};

struct oxtdrm_display_hints
{
    // Height and width of the display
    uint32_t width;
    uint32_t height;

    // X and Y position of the display
    uint32_t x;
    uint32_t y;

    // Display Handler key
    uint32_t key;

    // Recommended dumb buffer type (1920x1080 or 4k)
    enum oxtdrm_buffer_type buffer_type;
};

struct oxtdrm_kms_connector_group {
    // KMS structs
    struct drm_crtc crtc;
    struct drm_encoder encoder;
    struct drm_connector connector;

    // Crtc/encoder bitshift
    uint32_t index;

    // Planes
    struct drm_plane primary;
    struct drm_plane cursor;
    struct drm_plane overlay;
    uint32_t plane_count;
};

struct oxtdrm_kms_mapping {
    struct oxtdrm_kms_connector_group connector_group;

    // OXTDRM specifics
    struct oxtdrm_device *dev;
    struct oxtdrm_framebuffer *oxtfb;
    uint32_t key;
    uint32_t display_handle;

    struct pv_display *display;
    struct oxtdrm_display_hints display_hint;
    bool connected;
    bool connector_registered;
    bool pending_removal;
    enum oxtdrm_buffer_type buffer_type;
    bool buffer_needs_resize;

    struct hlist_node hash_node;
};

struct oxtdrm_device
{
    struct mutex lock;
    struct drm_device *dev;
    char name[16];

    struct workqueue_struct *reconnect_queue;

    // Full screen clear
    bool full_screen_clear;

    // Display handler specific stuff
    struct pv_display_provider *provider;
    int32_t configured_displays;
    int32_t advertised_display_count;

    // Display specific structs
    DECLARE_HASHTABLE(kms_map, OXTDRM_HASH_BUCKET_ORDER);
    void *cached_cursor;
    int32_t plane_count;
    int32_t cached_mapping;
    bool needs_reconnect;
    bool displays_needs_update;
    struct dh_remove_display *cached_request;
    int32_t dumb_buffer_width;
    int32_t dumb_buffer_height;
    enum oxtdrm_driver_status driver_status;
    bool host_layout_changed;
};

typedef struct {
    uint32_t num_displays;
    struct oxtdrm_display_hints display_hint[MAX_DISPLAYS];
    char connector_names[MAX_DISPLAYS][MAX_NAME_SIZE];
    bool is_rhel;
}query_display_info_t;

static
inline void __oxtdrm_kms_hash_add(struct oxtdrm_device *dev, struct oxtdrm_kms_mapping *mapping, uint32_t key)
{
    oxtdrm_debug(" ---> Adding %d to hash map\n", key);
    hash_add(dev->kms_map, &mapping->hash_node, key);
}

static
inline void __oxtdrm_kms_hash_del(struct oxtdrm_kms_mapping *mapping)
{
    if(mapping) {
      oxtdrm_debug(" <--- Removing %d from hash map\n", mapping->key);
      hash_del(&mapping->hash_node);
    }
}

static
inline void __oxtdrm_kms_hash_update_key(struct oxtdrm_device *dev, struct oxtdrm_kms_mapping *mapping, uint32_t key)
{
    oxtdrm_debug(" <--> Update key to 0x%08x hash map\n", key);
    hash_del(&mapping->hash_node);
    hash_add(dev->kms_map, &mapping->hash_node, key);
}

/* Helper Macro to convert a regular framebuffer object to a custom oxtdrm_fb */
#define to_oxtdrm_fb(x) container_of(x, struct oxtdrm_fb, base)

/* Functions */

int32_t oxtdrm_dumb_map_offset(struct drm_file *file_priv, struct drm_device *dev, uint32_t handle, uint64_t *offset);
int32_t oxtdrm_gem_mmap(struct file *filp, struct vm_area_struct *vma);
struct drm_gem_object* oxtdrm_gem_object_lookup(struct drm_device *dev, struct drm_file *file, uint32_t handle);
int32_t oxtdrm_dumb_create(struct drm_file *file, struct drm_device *dev, struct drm_mode_create_dumb *args);
int oxtdrm_dumb_destroy(struct drm_file *file_priv, struct drm_device *dev, uint32_t handle);

void oxtdrm_gem_free_object(struct drm_gem_object *object);
void oxtdrm_kms_cleanup(struct oxtdrm_kms_connector_group *connector_group);
int32_t oxtdrm_kms_init(struct oxtdrm_device *device);
void oxtdrm_tear_down_display(struct oxtdrm_device *device, struct pv_display *display);
int32_t oxtdrm_setup_kms_components(struct oxtdrm_device *device, struct drm_crtc *crtc, struct drm_encoder *encoder, struct drm_connector *connector);
int32_t oxtdrm_init_connector(struct drm_device *device, struct oxtdrm_kms_connector_group *connector_group);
int32_t oxtdrm_kms_init_connector_group(struct drm_device *device, struct oxtdrm_kms_connector_group *connector_group);
void oxtdrm_kms_mode_config_init(struct drm_device *dev);
int32_t oxtdrm_reinitialize_states(struct oxtdrm_kms_connector_group *connector_group);
int32_t oxtdrm_init_crtc(struct drm_device *device, struct oxtdrm_kms_connector_group *connector_group);
int32_t oxtdrmInitializeFb(struct oxtdrm_device *device);
void oxtdrm_update_display_connection_status(struct pv_display *display, bool connected, int32_t status);
void oxtdrm_fatal_error_handler(struct pv_display_provider *display);
void oxtdrm_handle_display_error(struct pv_display *display);
void oxtdrm_pv_remove_display(struct pv_display_provider *provider, struct dh_remove_display *request);
extern const struct drm_mode_config_funcs oxtdrm_mode_funcs;
extern const struct drm_mode_config_helper_funcs oxtdrm_drm_mode_config_helpers;
extern const struct drm_framebuffer_funcs oxtdrm_fb_funcs;
extern const struct drm_crtc_funcs oxtdrm_crtc_funcs;
extern const struct vm_operations_struct oxtdrm_gem_vm_ops;
extern const struct drm_gem_object_funcs oxtdrm_gem_object_funcs;

#endif // OXTDRM_IOCTL_H
