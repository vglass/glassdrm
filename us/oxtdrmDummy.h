#ifndef OXTDRMDUMMY_H
#define OXTDRMDUMMY_H

/* Includes */
#include <stdint.h>
#include <stddef.h>
#include <xf86drmMode.h>
#include <xf86drm.h>
#include <libdrm/drm.h>

/* Constants */
#define DEVICE_NAME "oxtdrm"
#define DEFAULT_MAX_CURSOR_WIDTH 64
#define DEFAULT_MAX_CURSOR_HEIGHT 64
#define FRAME_WIDTH  50
#define FRAME_LENGTH 50
#define FRAME_BPP 32
#define FRAME_DEPTH 24

/* Enable debug statements if DEBUG is declared */
#define oxtdrm_info(...) printf("oxtdrm[info]: " __VA_ARGS__)
#define oxtdrm_error(...) printf("oxtdrm[error]: "__VA_ARGS__)

typedef struct displayInfo
{
    drmVersionPtr version;
    drmModeResPtr resources;
    drmModeConnectorPtr connector;
    drmModeEncoderPtr encoder;
    drmModeCrtcPtr newCrtc;
    drmModeCrtcPtr oldCrtc;
    drmModeModeInfo mode;
    uint32_t width;
    uint32_t height;
    void *buffer;
    uint32_t fd;
    uint32_t framebufferId;
    uint32_t dumbBufferId;
    uint32_t dumbBufferSize;
    uint32_t dumbBufferPitch;
}displayInfo_t;  

/* Functions */
void displayVersionInformation(drmVersionPtr version);
drmVersionPtr getVersionInformation(int fd);
void setDeviceInformation(drmVersionPtr version);
uint32_t registerDevice(displayInfo_t *display);
uint32_t unregisterDevice(displayInfo_t *display);
uint32_t createDumbBuffer(displayInfo_t *display);
uint32_t *mapDumbBuffer(displayInfo_t *display);
uint32_t getFirstConnector(displayInfo_t *display);
uint32_t getEncorderForConnector(displayInfo_t *display);
uint32_t getCRTCForEncoder(displayInfo_t *display);
uint32_t testSetCrtc(displayInfo_t display);
uint32_t cleanupDrm(displayInfo_t *display);
uint32_t cleanupDisplayResources(displayInfo_t *display);

#endif // OXTDRMDUMMY_H
