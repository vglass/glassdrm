//#define _FILE_OFFSET_BITS 64

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include "oxtdrmDummy.h"

#define PLAYBACK_COUNT 3
#define DELAY 3

void displayVersionInformation(drmVersionPtr version)
{
    // Check for valid ptr
    if (!version)
    {
        oxtdrm_error("displayVersionInformation - Invalid Pointer");
        return;
    }

    // Print all of the information about the device
    oxtdrm_info("LibDRM Device Registered:\n");
    oxtdrm_info("-------------------------\n");
    oxtdrm_info("\t- version_major: %d\n", version->version_major);
    oxtdrm_info("\t- version_minor: %d\n", version->version_minor);
    oxtdrm_info("\t- version_pathLevel: %d\n", version->version_patchlevel);
    oxtdrm_info("\t- name: %s\n", version->name);
    oxtdrm_info("\t- date: %s\n", version->date);
    oxtdrm_info("\t- desc: %s\n", version->desc);

    return;
}

void displayResources(drmModeResPtr resources)
{
	// Check for valid ptr
	if (!resources)
	{
		oxtdrm_error("displayResources - Invalid resources ptr\n");
		return;
	}

    // Print all of the resource information
    oxtdrm_info("Resource Information\n");
    oxtdrm_info("--------------------\n");
	oxtdrm_info("\t- Number of currently allocated framebuffer objects: %d\n", resources->count_fbs);
	oxtdrm_info("\t- Number of available CRTC's: %d\n", resources->count_crtcs);
	oxtdrm_info("\t- Number of available connectors: %d\n", resources->count_connectors);
	oxtdrm_info("\t- Number of available encoders: %d\n", resources->count_encoders);
	oxtdrm_info("\t- Min Framebuffer Height: %d\n", resources->min_height);
	oxtdrm_info("\t- Max Framebuffer Height: %d\n", resources->max_height);
	oxtdrm_info("\t- Min Framebuffer Width: %d\n", resources->min_width);
	oxtdrm_info("\t- Max Framebuffer Width: %d\n", resources->max_width);

    return;
}

void displayConnectorInformation(drmModeConnectorPtr connector)
{
    // Check for valid ptr
    if (!connector)
    {
        oxtdrm_error("displayConnectorInformation - Invalid pointer to connector\n");
        return;
    }

    // Print all of the Connector information
    oxtdrm_info("Connector Information\n");
    oxtdrm_info("---------------------\n");
    oxtdrm_info("\t- Connector ID: %d\n", connector->connector_id);
    oxtdrm_info("\t- Encoder ID: %d\n", connector->encoder_id);
    oxtdrm_info("\t- Number of Encoders: %d\n", connector->count_encoders);

    oxtdrm_info("\t- Connection Status: ");
    if (connector->connection == DRM_MODE_CONNECTED)
        printf("Connected\n");
    else if (connector->connection == DRM_MODE_DISCONNECTED)
        printf("Disconnected\n");
    else
        printf("Unknown Connection Status\n");

    return;
}

void displayEncoderInformation(drmModeEncoderPtr encoder)
{
    // Check for valid ptr
    if (!encoder)
    {
        oxtdrm_error("displayEncoderInformation - Invalid pointer to encoder\n");
        return;
    }

    // Print all of the Encoder Information
    oxtdrm_info("Encoder Information\n");
    oxtdrm_info("-------------------\n");
    oxtdrm_info("\t- Encoder ID: %d\n", encoder->encoder_id);
    oxtdrm_info("\t- CRTC ID: %d\n", encoder->crtc_id);

    return;
}

void displayCrtcInformation(drmModeCrtcPtr crtc)
{
    // Check for valid ptr
    if (!crtc)
    {
        oxtdrm_error("displayCrtcInformation - Invalid pointer to crtc\n");
        return;
    }

    // Print all of the CRTC Information
    oxtdrm_info("CRTC Information\n");
    oxtdrm_info("----------------\n");
    oxtdrm_info("\t- CRTC ID: %d\n", crtc->crtc_id);
    oxtdrm_info("\t -CRTC Buffer ID: %d\n", crtc->buffer_id);

    return;
}

/* Obtain all of the version information about the DRM Driver */
drmVersionPtr getVersionInformation(int32_t fd)
{
    drmVersionPtr version = NULL;

    // Make sure that we have a valid file descriptor
    if (fd <= 0)
    {
        oxtdrm_error("getVersionInformation - Invalid file descriptor\n");
        return NULL;
    }

    /* Obtain and validate a drm version structure which contains information about the driver itself */
    version = drmGetVersion(fd);
    if (!version)
    {
        oxtdrm_error("getVersionInformation - Invalid version\n");
        return NULL;
    }

    return version;
}

uint32_t registerDevice(displayInfo_t *display)
{
    // Check for valid input
    if (display->fd <= 0)
    {
        oxtdrm_error("registerDevice: Invalid file descriptor\n");
        return 1;
    }

    // Obtain all of the version information
    display->version = getVersionInformation(display->fd);

    // Verify that the version information was properly set
    if (!(display->version))
    {
        oxtdrm_error("registerDevice - Invalid version Information\n");
        return 1;
    }

    return 0;
}

uint32_t unregisterDevice(displayInfo_t *display)
{
    // Free the drm version struct
    if (display->version)
        drmFreeVersion(display->version);

    drmDropMaster(display->fd);

    // Close DRM device
    if (drmClose(display->fd))
    {
        oxtdrm_error("Could not close DRM device\n");
        return 1;
    }

    return 0;
}

uint32_t createDumbBuffer(displayInfo_t *display)
{
    struct drm_mode_create_dumb create_arg = {0};


    //  These fields must be provided by the caller. The kernel will
    //  fill in the remaining fields.
    create_arg.width = display->width;
    create_arg.height = display->height;
    create_arg.bpp = FRAME_BPP;

    // Call the IOCTL to create the dumb buffer
    if (drmIoctl(display->fd, DRM_IOCTL_MODE_CREATE_DUMB, &create_arg))
    {
        oxtdrm_error("Failed: DRM_IOCTL_MODE_CREATE_DUMB - %s\n", strerror(errno));
        return 1;
    }

    //  The driver should fill in this information. The handle is a 32 bit gem handle
    //  that identifies the buffer. Most drivers use 32 or 64 bit aligned stride values. Size
    //  is the absolute size in bytes of the buffer
    display->dumbBufferSize = create_arg.size;
    display->dumbBufferPitch = create_arg.pitch;
    display->dumbBufferId = create_arg.handle;

    return 0;
}

uint32_t *mapDumbBuffer(displayInfo_t *display)
{
    uint32_t *addr = NULL;
    struct drm_mode_map_dumb map_arg = {0};
    map_arg.handle = display->dumbBufferId;

    if (drmIoctl(display->fd, DRM_IOCTL_MODE_MAP_DUMB, &map_arg))
    {
        oxtdrm_error("mapDumbBuffer - %s\n", strerror(errno));
        return NULL;
    }

    oxtdrm_error("map_arg.offset %p\n", (void*)map_arg.offset);

    if ((addr = (uint32_t *)mmap(0, display->dumbBufferSize, PROT_READ | PROT_WRITE, MAP_SHARED, display->fd, map_arg.offset)) == MAP_FAILED)
    {
        oxtdrm_error("mapDumbBuffer - Could not map the dumb buffer - %s\n", strerror(errno));
        addr = NULL;
        return NULL;
    }

    return addr;
}

void draw_buffer(char *addr, int width, int hieght, int pitch)
{
    int w, h;

    /* paint the buffer with colored tiles */
    for (h = 0; h < hieght / 4; h++)
     {
        uint32_t *fb_ptr = (uint32_t*)((char*)addr + h * pitch);

        for (w = 0; w < width / 4; w++)
        {
            fb_ptr[w] = 0x000000FF;
        }
    }
}

void displayAllConnectorsInformation(displayInfo_t display)
{
    int32_t i;
    drmModeConnector *connector;

    if (!display.resources)
    {
        oxtdrm_error("displayAllConnectorsInformation - Invalid pointer to resources\n");
        return;
    }

    for (i = 0; i < display.resources->count_connectors; i++)
    {
        connector = drmModeGetConnector(display.fd, display.resources->connectors[i]);
        displayConnectorInformation(connector);
        drmModeFreeConnector(connector);
    }

    return;
}

void displayAllCrtcsInformation(displayInfo_t display)
{
    int32_t i;
    drmModeCrtc *crtc;

    if (!display.resources)
    {
        oxtdrm_error("displayAllCrTcsInformation - Invalid pointer to resources\n");
        return;
    }

    for (i = 0; i < display.resources->count_crtcs; i++)
    {
        crtc = drmModeGetCrtc(display.fd, display.resources->crtcs[i]);
        displayCrtcInformation(crtc);
        drmModeFreeCrtc(crtc);
    }

    return;
}

uint32_t getFirstConnector(displayInfo_t *display)
{
    uint32_t i;

    // Validate input
    if (!display->resources)
    {
        oxtdrm_error("getFirstConnector - Invalid pointer detected\n");
        return 1;
    }

    // Iterate over all possible connectors until we find the first valid one
    for (i = 0; i < display->resources->count_connectors; i++)
    {
        // Grab connector
        display->connector = drmModeGetConnector(display->fd, display->resources->connectors[i]);
        if (display->connector)
        {
            displayConnectorInformation(display->connector);
            // Check to make sure that the connector is actually connected, and have a mode associated with it
            if ((display->connector)->connection == DRM_MODE_CONNECTED && (display->connector)->count_modes > 0)
                return 0;
            else
                drmModeFreeConnector(display->connector);
        }
    }

    // Finished iterating over all of the connectors, and was not able to find a valid one
    if (i == display->resources->count_connectors)
    {
        oxtdrm_error("getFirstConnector - Unable to find a valid connector\n");
        return 1;
    }

    return 0;
}

uint32_t getEncorderForConnector(displayInfo_t *display)
{
    uint32_t i;

    // Make sure pointers are valid
    if (!display->resources || !display->connector)
    {
        oxtdrm_error("getEncoderForConnector - Invalid connector detected\n");
        return 1;
    }

    // Iterate over all of the encoders, until we find one that is valid, and associated with the connector that is passed in
    for (i = 0; i < display->resources->count_encoders; i++)
    {
        display->encoder = drmModeGetEncoder(display->fd, display->resources->encoders[i]);
        if (display->encoder)
        {
            displayEncoderInformation(display->encoder);

            // Must get the encoder that is associated with the connector, not just any encoder
            if ((display->encoder)->encoder_id == display->connector->encoder_id)
                return 0;
            else
                drmModeFreeEncoder(display->encoder);
        }
    }

    // We iterated over all over the possible encoders and could not find the correct/valid one
    if (i == display->resources->count_encoders)
    {
        oxtdrm_error("getEncoderForConnector - Unable to find a valid encoder\n");
        return 1;
    }

    return 0;
}

uint32_t getCRTCForEncoder(displayInfo_t *display)
{
   uint32_t i;

    // Make sure pointers are valid
    if (!display->resources || !display->encoder)
    {
        oxtdrm_error("getCRTCForEncoder - Invalid pointer detected\n");
        return 1;
    }

    // Iterate over all possible CRTC and find the one that is attached
    // to the encoder.
    for (i = 0; display->resources->count_crtcs; i++)
    {
        display->newCrtc = drmModeGetCrtc(display->fd, display->resources->crtcs[i]);
        if (display->newCrtc)
        {
            displayCrtcInformation(display->newCrtc);

            // Get the CRTC that is associated with the encoder
            if ((display->newCrtc)->crtc_id == display->encoder->crtc_id)
                return 0;
            else
                drmModeFreeCrtc(display->newCrtc);
        }
    }

    // Check to see if no crtc's were found
    if (i == display->resources->count_crtcs)
    {
        oxtdrm_error("getCRTCForEncoder - Unable to find the associated CRTC\n");
        return 1;
    }

    return 0;
}
uint32_t testSetCrtcOnce(displayInfo_t display)
{

    printf("[oxtdrm]\nDisplay FD: %d\n New Crtc Id: %d\n FramebufferId %d\n ConnectorId %d\n Mode %d x %d\n",
                    display.fd,
                    display.newCrtc->crtc_id,
                    display.framebufferId,
                    display.connector->connector_id,
                    display.mode.hdisplay,
                    display.mode.vdisplay);

    // Set the CRTC
    if (drmModeSetCrtc(display.fd, display.newCrtc->crtc_id, display.framebufferId, 0, 0, &display.connector->connector_id, 1, &display.mode))
    {
            oxtdrm_error("Failed to set CRTC\n");
            perror("CRTC:");
            return 1;
    }

    printf("Display Width: %d,Display Height: %d,Display Pitch: %d\n", display.width, display.height, display.dumbBufferPitch);

    // Draw he image to the screen
    draw_buffer((char *)display.buffer, display.width, display.height, display.dumbBufferPitch);

    printf("Done Drawing\n");

    return 0;

}
uint32_t testSetCrtc(displayInfo_t display)
{
    uint32_t i;

    printf("[oxtdrm] Display FD: %d\n New Crtc Id: %d\n FramebufferId %d\n ConnectorId %d\n Mode %d x %d\n",
                    display.fd,
                    display.newCrtc->crtc_id,
                    display.framebufferId,
                    display.connector->connector_id,
                    display.mode.hdisplay,
                    display.mode.vdisplay);

    for (i = 0; i <= PLAYBACK_COUNT; i++)
    {
        // Set the CRTC
        if (drmModeSetCrtc(display.fd, display.newCrtc->crtc_id, display.framebufferId, 0, 0, &display.connector->connector_id, 1, &display.mode))
        {
            oxtdrm_error("Failed to set CRTC\n");
            perror("CRTC:");
            return 1;
        }

        // Draw he image to the screen
        draw_buffer((char *)display.buffer, display.width, display.height, display.dumbBufferPitch);

        // Freeze the image on the screen
        sleep(DELAY);

        // Restore CRTC
        if (drmModeSetCrtc(display.fd, display.oldCrtc->crtc_id, display.oldCrtc->buffer_id, display.oldCrtc->x, display.oldCrtc->y, &display.connector->connector_id, 1, &display.oldCrtc->mode))
        {
            oxtdrm_error("Failed to restore CRTC\n");
            perror("CRTC:");
            return 1;
        }

        sleep(DELAY);
    }

    return 0;
}

uint32_t cleanupDumbBuffers(displayInfo_t *display)
{
    // Validate input
    if (!display)
    {
        oxtdrm_error("cleanupDumbBuffers - Invalid pointer\n");
        return 1;
    }

    // Remove the framebuffer
    if (drmModeRmFB(display->fd, display->framebufferId))
    {
        oxtdrm_error("cleanupDumbBuffers - Could not remove buffer\n");
        return 1;
    }

    // Unmap the dumb buffer
    if (munmap(display->buffer, display->dumbBufferSize))
    {
        oxtdrm_error("cleanupDumbBuffers - Could not unmap memory\n");
        return 1;
    }

    // Remove the dumb buffer
    if (drmIoctl(display->fd, DRM_IOCTL_MODE_DESTROY_DUMB, &display->dumbBufferId))
    {
        oxtdrm_error("cleanupDumbBuffers - Unable to destroy dumb buffer\n");
        return 1;
    }

    return 0;
}

uint32_t cleanupDisplayResources(displayInfo_t *display)
{
    // Validate Input
    if (!display)
    {
        oxtdrm_error("cleanupDisplayResources - Invalid pointer\n");
        return 1;
    }

    // Freeup the resources that was allocated to the new CRTC
    if (display->newCrtc)
        drmModeFreeCrtc(display->newCrtc);

    // Freeup the resources that was allocated to the saved CRTC
    if (display->oldCrtc)
        drmModeFreeCrtc(display->oldCrtc);

    // Freeup the resources that was allocated for the encoder
    if (display->encoder)
        drmModeFreeEncoder(display->encoder);

    // Freeup the resources that was allocated for the connector
    if (display->connector)
        drmModeFreeConnector(display->connector);

    return 0;
}

uint32_t cleanupDrm(displayInfo_t *display)
{
    // Validate Input
    if (!display)
    {
        oxtdrm_error("cleanupDrm - Invalid pointer\n");
        return 1;
    }

    // Freeup  and unmap the dumb buffers that were allocated
    if (cleanupDumbBuffers(display))
        return 1;

    // Freeup connector, encoder, and crtc resources
    if (cleanupDisplayResources(display))
        return 1;

    // Cleanup
    if (unregisterDevice(display))
        return 1;

    return 0;
}


// TODO: Write a function that will print all of the display information that is being used

int32_t main(void)
{
    displayInfo_t display = {0};

	// Determine if DRM driver is loaded
    if (!drmAvailable())
    {
        oxtdrm_error("DRM driver is not loaded.\n");
    	return 1;
    }

    // Open the main drm contorl device and obtain a file descriptor for it
    if ((display.fd = open("/dev/dri/card0", O_RDWR)) < 0)
    {
        oxtdrm_error("Could not open the main drm control device\n");
    	return 1;
    }

    // Give this application master privilege over drm driver
    drmSetMaster(display.fd);

    // Register the drm device by obtaining the version information and update the device information
    if (registerDevice(&display))
    {
        unregisterDevice(&display);
        return 1;
    }

	// Get all of the resource Information
	display.resources = drmModeGetResources(display.fd);
	if (!display.resources)
	{
		oxtdrm_error("Unable to obtain resources from the drm driver\n");
        unregisterDevice(&display);
        return 1;
	}

    // Print version Information
    displayVersionInformation(display.version);

    // Print All of the resource information
	displayResources(display.resources);

    // Grab the first valid connector available
    if (getFirstConnector(&display))
        goto error1;

    // Grab the encoder that is associated with the connector
    if (getEncorderForConnector(&display))
        goto error2;

    // Grab the CRTC that is associated with the encoder
    if (getCRTCForEncoder(&display))
        goto error2;

    // Update mode information
    display.mode = display.connector->modes[0];
    display.width = display.mode.hdisplay;
    display.height = display.mode.vdisplay;

    // Get dumb buffer created using the modes width and height
    if (createDumbBuffer(&display))
        goto error2;

    oxtdrm_info("Dumb Buffer Created\n");


    // Get framebuffer id for the dumb buffer
    if (drmModeAddFB(display.fd, display.width, display.height, FRAME_DEPTH, FRAME_BPP, display.dumbBufferPitch, display.dumbBufferId, &display.framebufferId))
    {
        oxtdrm_error("drmModeAddFB - %s\n", strerror(errno));
        goto error2;
    }

    // Map the dumb buffer that was created
    display.buffer = mapDumbBuffer(&display);
    if (display.buffer == NULL)
        goto error3;

    // Back up the old CRTC, so it can be restored later
    display.oldCrtc = drmModeGetCrtc(display.fd, display.encoder->crtc_id);

    // Testing Writing to and restoring the CRTC
    //if (testSetCrtc(display))
    //    goto error3;

    if (testSetCrtcOnce(display))
        goto error3;

    // Perform all cleanup, unmap and remove buffers and cleanup resources
    if (cleanupDrm(&display))
        goto error3;

    oxtdrm_info("All Done\n");

    return 0;

error1:
    unregisterDevice(&display);
    return 1;

error2:
    cleanupDisplayResources(&display);
    unregisterDevice(&display);
    return 1;

error3:
    cleanupDrm(&display);
    return 1;
}
