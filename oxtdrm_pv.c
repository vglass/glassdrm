#include "oxtdrm_pv.h"

extern struct oxtdrm_device *oxtdrm_dev;

static int32_t oxtdrm_create_framebuffer(struct oxtdrm_device *device, struct dh_add_display *request);

void
__print_mapping_info(struct oxtdrm_device *device)
{
    int32_t i, j = 0;
    struct oxtdrm_kms_mapping *mapping = NULL;

    if (!device) {
        return;
    }

    oxtdrm_debug("******************************* Mapping Info********************************\n");
    hash_for_each(device->kms_map, i, mapping, hash_node) {
        if(!mapping) continue;
        oxtdrm_debug("Display[%d] - Key: 0x%08x, res %dx%d x,y %d,%d\n", j, mapping->key, mapping->display_hint.width, mapping->display_hint.height, mapping->display_hint.x, mapping->display_hint.y);
        j++;
    }
    oxtdrm_debug("****************************************************************************\n");
}

void
__print_connector_group_ids(struct oxtdrm_kms_connector_group group)
{
    oxtdrm_debug("Connector_Group: Connector ID %d -> Encoder ID %d -> CRTC ID %d\n", group.connector.base.id, group.encoder.base.id, group.crtc.base.id);
}

static void
recreate_provider(struct work_struct *work)
{
    int rc = 0;

    // Use container_of instead!
    struct oxtdrm_work *w = (struct oxtdrm_work *)work;
    struct oxtdrm_device *oxtdrm_dev = w->device;

    __OXTDRM_TRACE__;

    if(!oxtdrm_dev) {
        return;
    }

    // If we have an existing provider, tear it down.
    if (oxtdrm_dev->provider) {
        oxtdrm_teardown_provider();
    }

    // Create our connection to the display handler
    rc = create_pv_display_provider(&oxtdrm_dev->provider, DOMAIN_0, DISPLAY_HANDLER_PORT);

    // If we were able to make a connection, initialize the provider.
    if (!rc) {
      if (!oxtdrm_dev->provider)
          return;

      // Register the host display change handler. This will be called
      // whenever there is a new or changed display
      oxtdrm_dev->provider->register_host_display_change_handler(oxtdrm_dev->provider,
        oxtdrm_host_display_changed);

      // Register the add display request handler.
      oxtdrm_dev->provider->register_add_display_request_handler(oxtdrm_dev->provider,
        oxtdrm_add_display_request);

      // Register our display fatal error handler that will handle issues in
      // regards to the display connection
      oxtdrm_dev->provider->register_fatal_error_handler(oxtdrm_dev->provider,
        (fatal_provider_error_handler)oxtdrm_reconnect_provider);

      // Register the remove display request handler
      oxtdrm_dev->provider->register_remove_display_request_handler(oxtdrm_dev->provider,
        oxtdrm_pv_remove_display);

      // Set the owner of this provider. This will allow us
      // to access fields in the owner via the provider.
      oxtdrm_dev->provider->owner = (void *)oxtdrm_dev;

      // Advertise the PV Driver's capabilities to the display handler
      oxtdrm_dev->provider->advertise_capabilities(oxtdrm_dev->provider, MAX_DISPLAYS);

      w->device = NULL;
      kfree((void *) work);
  } else {
      queue_work(oxtdrm_dev->reconnect_queue, (struct work_struct *)work);
    }
}

int32_t
oxtdrm_reconnect_provider(struct pv_display_provider *provider)
{
    struct oxtdrm_work *work = NULL;
    (void) provider;

    __OXTDRM_TRACE__;

    if (!oxtdrm_dev)
        return 1;
    // We were not able to make a connection, wait for a
    // delay, then try again.
    work = kzalloc(sizeof(*work), GFP_KERNEL);

    INIT_WORK((struct work_struct *) work, recreate_provider);
    work->device = oxtdrm_dev;

    queue_work(oxtdrm_dev->reconnect_queue, (struct work_struct *)work);

    return 0;
}

/**
* Update the display connection status, for example in the case of an unexpected host disconnect
* or following a reconnect operation.
*
* @param display pv_display object to update connection status for
* @param connected Set connection status
*/
void
oxtdrm_update_display_connection_status(struct pv_display *display, bool connected, int32_t status)
{
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    if (!display) {
        return;
    }

    if (status == OXTDRM_STATUS_BUSY) {
        oxtdrm_dev->driver_status = OXTDRM_STATUS_BUSY;
    }

    mutex_lock(&oxtdrm_dev->lock);

    hash_for_each_possible(oxtdrm_dev->kms_map, mapping, hash_node, display->key) {
        if (!mapping || !mapping->display || mapping->connected == connected) {
            continue;
        }

        mapping->connected = connected;
        if (connected) {
            mapping->connector_group.connector.status = connector_status_connected;
            oxtdrm_dev->configured_displays++;
        } else {
            mapping->connector_group.connector.status = connector_status_disconnected;
            oxtdrm_dev->configured_displays--;
        }

        break;
    }

    mutex_unlock(&oxtdrm_dev->lock);
}

void
oxtdrm_teardown_provider(void)
{
    __OXTDRM_TRACE__;
    if (!oxtdrm_dev) {
      return;
    }
    //
    // drm_mode_config_cleanup(oxtdrm_dev->dev);

    // Teardown all displays
    oxtdrm_destroy_displays();

    // // Teardown the provider
    // if(oxtdrm_dev->provider) {
    //   oxtdrm_dev->provider->destroy(oxtdrm_dev->provider);
    //   oxtdrm_dev->provider = NULL;
    // }

    return;
}

void
oxtdrm_destroy_displays(void)
{
    int32_t i = 0;
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    // If we do not have a valid context struct or
    // there arent any configured_displays then we can not do anything.
    if (!oxtdrm_dev || !oxtdrm_dev->configured_displays) {
        return;
    }

    // Iterate over each mapping in the list, if the mapping exist then destroy the display
    hash_for_each(oxtdrm_dev->kms_map, i, mapping, hash_node) {
        if (!mapping || !mapping->display || mapping->key < 0) {
            continue;
        }

        // If we did have a valid mapping, display and key, but the mapping is
        // marked ad being inactive, then remove that display
        if (mapping->connected == false) {
            oxtdrm_destroy_display(mapping->key);
        }
    }

    // Our list should now be up to date
    oxtdrm_dev->displays_needs_update = false;
}

void
oxtdrm_destroy_display(uint32_t key)
{
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    // If there are no configured_displays or if we have an empty key, then our work is already done.
    if (!oxtdrm_dev->configured_displays || key < 0) {
        return;
    }

    // Iterate over the list until we find the key to be removed.
    hash_for_each_possible(oxtdrm_dev->kms_map, mapping, hash_node, key) {
        if (!mapping || !mapping->display || mapping->pending_removal == false) {
            continue;
        }

        mapping->pending_removal = false;
    }
}

static int32_t
__validate_new_mode(struct oxtdrm_kms_mapping *mapping)
{
    __OXTDRM_TRACE__;

    if (!mapping) {
        return -ENOENT;
    }

    /* If the buffer is already a 4k buffer, then the new mode should be able to fit */
    if (mapping->buffer_type == OXTDRM_BUFFER_4K) {
        if (mapping->buffer_needs_resize) {
            return 1;
        }
    } else if (mapping->buffer_type == OXTDRM_BUFFER_1080) {
        /* Check to see if the new mode is greater than a 1920x1080 buffer
           if it is, then mark the display as stale so a new dumb buffer gets created
        */
        if (mapping->display_hint.width > MODE_HD_WIDTH || mapping->display_hint.height > MODE_HD_HEIGHT) {
            mapping->buffer_needs_resize = true;
            return 1;
        }
    }

    return 0;
}

static int32_t
__update_display_info_from_hint(struct pv_display *display)
{
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    if (!oxtdrm_dev || !display) {
        return -ENOENT;
    }

    /* Find the mapping that is attached to the display, and update the display info */
    hash_for_each_possible(oxtdrm_dev->kms_map, mapping, hash_node, display->key) {
        if (!mapping || !mapping->display) {
            continue;
        }

        __print_mapping_info(oxtdrm_dev);

       /* Check to ensure that the new mode can fit into the current buffer size
          if it can, then update width, height and stride. Otherwise leave it alone.*/
        if (__validate_new_mode(mapping)) {
            return 1;
        }
    }

    return 0;
}

int32_t
oxtdrm_reconnect_display_to_backend(struct pv_display *display, struct dh_add_display *request)
{
    struct oxtdrm_kms_mapping *mapping = NULL;
    int32_t rc = 0;

    __OXTDRM_TRACE__;

    if (!display || !request || !oxtdrm_dev) {
        return -EINVAL;
    }

    // The key may have changed, update it
    display->key = request->key;

    __update_display_info_from_hint(display);

    /* Find the mapping that is attached to the display, and update the display info */
    hash_for_each_possible(oxtdrm_dev->kms_map, mapping, hash_node, display->key) {
        if (!mapping || !mapping->display) {
            continue;
        }

        if (mapping->buffer_needs_resize) {
            oxtdrm_create_framebuffer(oxtdrm_dev, request);
        }
    }

    // Request a new connection from displayhandler to this display
    rc = display->reconnect(display, request, DOMAIN_0);
    if (unlikely(rc)) {
        oxtdrm_error("Failed to reconnect to display: %u\n", (uint32_t)request->key);
        return rc;
    }

    // The display is now live, set it to connected and update the number of configured displays
    oxtdrm_update_display_connection_status(display, true, OXTDRM_STATUS_INITIALIZED);

    rc = display->change_resolution(display, display->width, display->height, display->stride);
    if(rc) {
        oxtdrm_error("Failed to change resolution for display: %d. Error %d", display->key, rc);
    }

    display->invalidate_region(display, 0, 0, display->width, display->height);
    display->set_cursor_visibility(display, display->cursor.visible);

    return 0;
}

static void
__oxtdrm_populate_display_hints(struct oxtdrm_display_hints *display_hints,
                               struct dh_display_info display_info)
{

    __OXTDRM_TRACE__;

    if (!display_hints) {
        return;
    }

    oxtdrm_debug("Hints for [0x%08x] WidthxHeight %dx%d x,y %d,%d", display_info.key, display_info.width, display_info.height, display_info.x, display_info.y);

    // Store the hints that were supplied by the display handler
    // so they can be applied later
    display_hints->width  = display_info.width;
    display_hints->height = display_info.height;
    display_hints->x      = display_info.x;
    display_hints->y      = display_info.y;
    display_hints->key    = display_info.key;
}

/*
static bool
__is_key_in_local_list(struct oxtdrm_device *device, uint32_t key)
{
    struct oxtdrm_kms_mapping *mapping = NULL;

    if (!device) {
        return false;
    }

    hash_for_each_possible(device->kms_map, mapping, hash_node, key) {
        if (!mapping || !mapping->display) {
            continue;
        }
        return true;
    }

    return false;
}
*/

static bool
__is_key_in_list(struct oxtdrm_device *device, struct dh_display_info *displays,
                 uint32_t num_displays, uint32_t existing_key)
{
    int32_t i;

    __OXTDRM_TRACE__;

    if (!device || !displays || !num_displays || (existing_key & 0x80000000)) {
       return false;
    }

    // Iterate over the list of displays, and check to see if our existing_key
    // lives in that display list
    for (i = 0; i < num_displays; ++i) {
        if (existing_key == displays[i].key) {
            return true;
        }
    }

    oxtdrm_debug("Key %u is no longer in use\n", (uint32_t)existing_key);
    return false;
}

/**
* oxtdrm_validate_existing_display_list
*
* Compares our local list of displays againse an incoming list of displays.
* Any display in our local list that is no longer in the new incoming list
* will be marked as inactive so its resources can be removed later on.
*
* @param device       - oxtdrm context struct
*
* @param displays     - A pointer to an array of dh_display_info structures,
*                       which describe the host displays and their layout.
* @param num_displays - The number of entries in the displays array.
*/
static void
oxtdrm_validate_existing_display_list(struct oxtdrm_device *device,
                                     struct dh_display_info *displays,
                                     uint32_t num_displays)
{
    struct oxtdrm_kms_mapping *mapping = NULL;
    int32_t i;

    __OXTDRM_TRACE__;

    // Validate parameters
    if (!device || !displays || !num_displays) {
        return;
    }

    // Iterate over each item in the hash map, then check to see if any
    // of our existing displays no longer reside in the new list of displays.
    hash_for_each(device->kms_map, i, mapping, hash_node) {
        if (!mapping || (mapping->key & 0x80000000)) {
            continue;
        }

        // Iterate over each display in the incoming display list to see
        // If the current mapping key resides in the list. If it does not, then we
        // no longer need it's services
        if (__is_key_in_list(device, displays, num_displays, mapping->key) == false) {
            oxtdrm_update_display_connection_status(mapping->display, false, OXTDRM_STATUS_INITIALIZED);
            mapping->pending_removal = true;
            device->displays_needs_update = true;
        }
    }
}

static void
__set_buffer_size_per_display(struct oxtdrm_kms_mapping *mapping, struct dh_display_info display)
{
    __OXTDRM_TRACE__;

    if (!mapping) {
        return;
    }

    /* If the mapping has already crossed the 4K boundary, then leave it as 4k. */
    if (mapping->buffer_type == OXTDRM_BUFFER_4K) {
        return;
    }

    if (display.width <= MODE_HD_WIDTH && display.height <= MODE_HD_HEIGHT) {
        mapping->buffer_type = OXTDRM_BUFFER_1080;
    } else if (display.width <= MODE_4K_WIDTH && display.height <= MODE_4K_HEIGHT) {
        /* If the buffer was previously marked as a 1080 buffer, then it will need resizing */
        if (mapping->buffer_type == OXTDRM_BUFFER_1080) {
            mapping->buffer_needs_resize = true;
        }
        mapping->buffer_type = OXTDRM_BUFFER_4K;
    } else {
        oxtdrm_error("Resolution is not supported. Defaulting to 1920x1080");
        mapping->buffer_type = OXTDRM_BUFFER_1080;
    }

    return;
}

static void
__calculate_dumb_buffer_size(struct oxtdrm_device *device)
{
    struct oxtdrm_kms_mapping *mapping = NULL;
    uint32_t mode_1080_count = 0, mode_4k_count = 0, i = 0;


    __OXTDRM_TRACE__;

    if (!device) {
        return;
    }

    hash_for_each(device->kms_map, i, mapping, hash_node) {
        if (!mapping || (mapping->key & 0x80000000)){
            continue;
        }

        if (mapping->buffer_type == OXTDRM_BUFFER_1080) {
            mode_1080_count++;
        } else {
            mode_4k_count++;
        }
    }

    device->dumb_buffer_width  = (MODE_HD_WIDTH * mode_1080_count) + (MODE_4K_WIDTH * mode_4k_count);
    device->dumb_buffer_height = mode_4k_count ? MODE_4K_HEIGHT : MODE_HD_HEIGHT;

    oxtdrm_debug("Calculated a dumb buffer size of %dx%d\n", device->dumb_buffer_width, device->dumb_buffer_height);
    return;
}

/**
* oxtdrm_host_display_changed
*
* Handles the host display list event. This is where the host sends
* a list of the displays that it wants the pv driver to handle. This is used
* as a hint for the driver's advertise_displays callback.
*
* @param provider     - The display provider for which the host displays
*                       are changing.
* @param displays     - A pointer to an array of dh_display_info structures,
*                       which describe the host displays and their layout.
* @param num_displays - The number of entries in the displays array.
*/
void
oxtdrm_host_display_changed(struct pv_display_provider *provider,
                           struct dh_display_info *displays,
                           uint32_t num_displays)
{
    int32_t i = 0, j = 0;
    struct oxtdrm_device *device = NULL;
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    // Validate parameters
    if (!provider || !displays || num_displays > MAX_DISPLAYS) {
        return;
    }

    // Get our oxtdrm_device from the owner, so we can
    // populate some of its fields
    device = (struct oxtdrm_device *)provider->owner;
    if (!device) {
        return;
    }

    oxtdrm_debug("Incoming number of displays: %d\n", num_displays);

    if (device->configured_displays == num_displays && !device->displays_needs_update) {
        return;
    }
 
    // Used for tracking the number of displays that glass reports to us.
    // This will be used later to ensure that we configure all of the displays
    device->advertised_display_count = num_displays;

    mutex_lock(&device->lock);
    for (i = 0; i < num_displays; ++i) {
        mapping = NULL;
        j = 0;

        oxtdrm_debug("Processing Key: 0x%08x\n", displays[i].key);

        // Check if we have an existing mapping for the current key, if so just update it's info
        hash_for_each(device->kms_map, j, mapping, hash_node) {
            if (mapping && (mapping->key == displays[i].key)) {
                oxtdrm_debug("Found an existing mapping for key [0x%08x]\n", mapping->key);
                goto reinit;
            }
        }

        // Get the next unused mapping (who's key is set to -1) to be used for this new display
        hash_for_each(device->kms_map, j, mapping, hash_node) {
            if (mapping && (mapping->key & 0x80000000)) {
                mapping->key = displays[i].key;
                oxtdrm_debug("Updated mapping with key: [0x%08x]\n", mapping->key);
                break;
            }
        }

reinit:
        __oxtdrm_kms_hash_update_key(device, mapping, displays[i].key);
        __oxtdrm_populate_display_hints(&mapping->display_hint, displays[i]);
        __set_buffer_size_per_display(mapping, displays[i]);
    }

    __calculate_dumb_buffer_size(device);
    mutex_unlock(&device->lock);

    // Check our local list against the incoming list to see if we have any stale displays
    oxtdrm_validate_existing_display_list(device, displays, num_displays);

    // Inform the display handler that we accept the displays
    provider->advertise_displays(provider, displays, num_displays);

    return;
}

/**
 * Returns true iff we should reconnect the given display.
 *
 * @param existing_display The display to be considered for reconnection...
 * @param request The add_display request that should be considered.
 */
static bool
__add_display_requires_reconnect(struct pv_display *existing_display, struct dh_add_display *request, bool connected)
{
    __OXTDRM_TRACE__;
    return (request->key != existing_display->key) || !connected;
}

/**
 * Attempts to located an existing pv_display object based on its key.
 *
 * @param instance The platforms instance that should own the given
 *    pv_display object.
 * @param key The Display Handler key identifying the display to locate.
 * @param connected Set to true if the given display was marked as connected,
 *    or false if the given display was marked as disconnected.
 * @return The located pv_display object, or NULL if none was found.
 */
static struct pv_display *
__find_add_display_target(struct oxtdrm_device *device, uint32_t key, bool *connected)
{
    struct pv_display *target = NULL;
    struct oxtdrm_kms_mapping *mapping = NULL;

    __OXTDRM_TRACE__;

    if (!device || !connected) {
        return NULL;
    }

    *connected = false;

    mutex_lock(&device->lock);
    hash_for_each_possible(device->kms_map, mapping, hash_node, key) {
        if (!mapping) continue;

        if (mapping && mapping->key == key) {
            target = mapping->display;
            *connected = mapping->connected;
            break;
        }
    }
    mutex_unlock(&device->lock);

    return target;
}

void
__print_connector_group_info(const struct oxtdrm_kms_mapping *mapping)
{
    if (!mapping) {
        return;
    }

    oxtdrm_debug("Group info for key [%d]. CRTC ID: %d, Encoder ID: %d, Connector ID: %d\n",
                mapping->key,
                mapping->connector_group.crtc.base.id,
                mapping->connector_group.encoder.base.id,
                mapping->connector_group.connector.base.id);
}

void
__print_create_framebuffer_request(const struct pv_display *display, const struct dh_add_display *request, struct oxtdrm_display_hints *hint)
{
    if (!request) {
        return;
    }

    if (!display && hint) {
        oxtdrm_debug("Host wants to add display %u with resolution %ux%u, stride %lu event port %u,"
                    "framebuffer port %u, dirty rect port %u, cursor port %u.\n",
                    request->key, hint->width, hint->height, PAGE_ALIGN(pixels_to_bytes(hint->width)),
                    request->event_port, request->framebuffer_port,
                    request->dirty_rectangles_port, request->cursor_bitmap_port);
    } else {
        if (hint) {
            oxtdrm_debug("Resolution successfully changed for display: %d, with resolution %dx%d, Event Port %u,"
                        "Framebuffer Port: %u, Dirty Rect Port: %u, Cursor Port: %u\n",
                        display->key, display->width, display->height, request->event_port, request->framebuffer_port,
                        request->dirty_rectangles_port, request->cursor_bitmap_port);
        }
    }
}

/**
 * Creates a framebuffer, allocating and registering all relevant objects.
 * Results in a fully working framebuffer accessible under /dev/fbX.
 *
 * @param device oxtdrm context struct
 *
 * @param request The PV display request which describes the display to be
 *                created.
 */
static int32_t
oxtdrm_create_framebuffer(struct oxtdrm_device *device, struct dh_add_display *request)
{
    struct oxtdrm_kms_mapping *mapping = NULL;
    struct pv_display_provider *provider = NULL;
    struct oxtdrm_display_hints *hint = NULL;
    struct pv_display *display = NULL;
    uint32_t stride, width, height;
    int32_t rc = 0, i = 0;

    __OXTDRM_TRACE__;

    // Verify parameters
    if (!device || !device->provider || !request) {
        return -EINVAL;
    }

    // Get a shortcut to the provider
    provider = device->provider;

    // Get the mapping thats associated with the ID
    hash_for_each(oxtdrm_dev->kms_map, i, mapping, hash_node) {
        if (!mapping || mapping->key != request->key) {
            continue;
        }

        hint = &mapping->display_hint;
        if (!hint) {
            return -EINVAL;
        }

        height = mapping->buffer_type == OXTDRM_BUFFER_1080 ? MODE_HD_HEIGHT : MODE_4K_HEIGHT;
        width  = mapping->buffer_type == OXTDRM_BUFFER_1080 ? MODE_HD_WIDTH : MODE_4K_WIDTH;

        // Calculate the stride for the framebuffer, by making it paged align it
        // improves the speed of copying the framebuffer in and out.
        stride = PAGE_ALIGN(pixels_to_bytes(width));

        // Create the initial pv framebuffer
        rc = provider->create_display(provider, &mapping->display, request, width, height, stride, NULL);
        if (rc || !mapping->display) {
            oxtdrm_error("Failed to create pv_display[0x%08x]: %p, %d\n", request->key, mapping->display, rc);
            return -rc;
        }

        oxtdrm_debug("Successfully created a display for key [0x%08x]\n", request->key);

        display = mapping->display;
        mutex_lock(&device->lock);
        // Update per display bookeeping. TODO: Put the below bits in a function
        display->register_fatal_error_handler(mapping->display, oxtdrm_handle_display_error);
        display->set_driver_data(display, mapping);
        mapping->connector_group.crtc.dev = device->dev;
        mapping->connector_group.crtc.enabled = true;
        mapping->connector_group.crtc.state->enable = true;
        mapping->connector_group.connector.status = connector_status_connected;
        mapping->connected = true;
        mapping->buffer_needs_resize = false;
        mutex_unlock(&device->lock);

        __print_create_framebuffer_request(display, request, hint);
        device->configured_displays++;
    }
    return rc;
}

/**
  oxtdrm_add_display_request
 *
 * Handles an Add Display Request, in which the host sends connection
 * information for a new connection, and requests that the PV Driver connect
 * to it, and begin providing a new framebuffer (and asociated communications.)
 *
 * @param display - The display provider which has receievd the
*                   Add Display request.
 * @param request - The Add Display Request, which contains the information
 *                  necessary to connect to the given display.
 */
void
oxtdrm_add_display_request(struct pv_display_provider *provider, struct dh_add_display *request)
{
    struct oxtdrm_device *device = NULL;
    struct pv_display *existing_display = NULL;
    bool connected, requires_reconnect;
    int32_t rc = 0;
    static int32_t processed_displays = 0;

    __OXTDRM_TRACE__;

    // Validate Parameters
    if (!provider || !request) {
        return;
    }

    // Get the owner of the device
    device = (struct oxtdrm_device *)provider->owner;
    if (!device) {
        return;
    }

    // Check to see if we already have a display associated with the request's unique key
    // If we already have a display that can render the existing connection
    existing_display = __find_add_display_target(device, request->key, &connected);
    if (existing_display) {
        // Check to see if we'll need to reconnect.
        requires_reconnect = __add_display_requires_reconnect(existing_display, request, connected);

        // If set-up for the relevant display would require a reconnect (e.g. because
        // we've lost connection, or the connection details have changed), reconnect
        // to the provided IVC ports.
        if (requires_reconnect) {
            oxtdrm_reconnect_display_to_backend(existing_display, request);
        } else {
            oxtdrm_debug("Connection already established with display, just updating it...\n");

            // Otherwise, the display handler is sending us a display for which we already
            // have a connection. In this case, we re-affirm our resolution$. the DH.
            rc = existing_display->change_resolution(existing_display, existing_display->width, existing_display->height, existing_display->stride);
            if (unlikely(rc)) {
                return;
            }
            __print_create_framebuffer_request(existing_display, request, NULL);
        }
    } else {
        oxtdrm_create_framebuffer(device, request);
    }

    // It we detected any displays as being stale, remove them
    if (device->displays_needs_update) {
        oxtdrm_debug("Detected stale displays. Purging....\n");
        oxtdrm_destroy_displays();
    }

    // Wait until all displays are setup, then send the hotplug event
    if (++processed_displays == device->advertised_display_count) { 
        drm_sysfs_hotplug_event(device->dev);
        processed_displays = 0;
        device->driver_status = OXTDRM_STATUS_READY;
    }

    return;
}
