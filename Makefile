export KERNEL_SRC ?= /lib/modules/$(shell uname -r)/build
export RPM_OUTDIR ?= $(shell pwd)

IVC_INCLUDE_DIR ?= $(KDIR)/include
DDH_INCLUDE_DIR ?= $(KDIR)/include

ccflags-y := -I/usr/include -ldrm -Iinclude/drm -I$(IVC_INCLUDE_DIR) -I$(DDH_INCLUDE_DIR) -g

# Build the oxtdrm module
oxtdrm-y := oxtdrm_drv.o oxtdrm_fb.o oxtdrm_gem.o oxtdrm_kms.o oxtdrm_pv.o
obj-m += oxtdrm.o

all: modules

modules:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) modules

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) modules_install

deb:
	dpkg-buildpackage -b -uc

deb-dkms:
	dpkg-buildpackage -A -uc

rpm:
	fpm -s dir -t rpm -n oxtdrm -v 1.0 -p "$(RPM_OUTDIR)" -a all --prefix /usr/src/oxtdrm-1.0/ -d "dkms" -d "ddh >= 1.0" -d "ivc >= 1.0" -d "libwayland-client" -d "libwayland-server" -d "libwayland-cursor" --after-install rpmdkms/dkmspostinstall --before-remove rpmdkms/dkmspreuninstall --after-remove rpmdkms/dkmspostuninstall --vendor AIS -m AIS@ainfosec.com dkms.conf Makefile oxtdrm_drv.c oxtdrm_drv.h oxtdrm_fb.c oxtdrm_gem.c oxtdrm_kms.c oxtdrm_pv.h oxtdrm_pv.c oxtdrm.conf 60-oxtdrm.conf blacklist-bochs_drm.conf 10-oxtdrm.rules display_hotplug.sh

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) clean
	rm -f *.rpm
