#!/bin/bash

RIP=$(dmesg | grep RIP | egrep -o [a-f0-9]{16} | uniq | tr '[a-z]' '[A-Z]')
MODULE_BASE=$(sudo cat /proc/modules | grep ^oxtdrm | egrep -o [a-f0-9]{16} | tr '[a-z]' '[A-Z]')
ARGS="obase=16;ibase=16;$RIP-$MODULE_BASE"
OUTPUT=$(echo $ARGS | bc)
addr2line -e oxtdrm.ko 0x$OUTPUT


